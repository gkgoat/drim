trait Obj{
    fn ty(&self) -> ObjTy;
    fn ty_static() -> ObjTy;
    fn null() -> Box<Self>;
}
#[derive(PartialEq)]
enum ObjTy{
    Int,
    Slice,
    Fn,
    Id(Vec<String>)
}
impl Obj for u64{
    fn ty(&self) -> ObjTy{
        return ObjTy::Int;
    } 
    fn ty_static() -> ObjTy{
        return ObjTy::Int;
    }
    fn null() -> Box<Self>{
        return Box::new(0);
    }
}
impl Obj for [dyn Obj]{
    fn ty(&self) -> ObjTy{
        return ObjTy::Slice;
    }
    fn ty_static() -> ObjTy{
        return ObjTy::Slice;
    }
    fn null() -> Box<Self>{
        return Box::new([]);
    }
}
type Fun = dyn FnMut(&dyn Obj,&mut dyn Obj);
impl Obj for Fun{
    fn ty(&self) -> ObjTy{
        return ObjTy::Fn;
    }
    fn ty_static() -> ObjTy{
        return ObjTy::Fn;
    }  
    fn null() -> Box<Self>{
        return Box::new(|a,b|{})
    } 
}
unsafe fn to<T: Obj>(x: dyn Obj) -> Result<T,Box<dyn Obj>>{
    if x.ty() != T::ty_static(){
        return Err(Box::new(x));
    }
    return Ok(unsafe{
        std::mem::transmute(x)
    })
}
fn box_(x: impl Obj) -> Box<dyn Obj>{
    return Box::new(x);
}
macro_rules! dynamic{
    ($ ($x:expr )) => {
        *box_(x)
    }
}