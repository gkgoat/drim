// //! Backend for generation of Rust code


// /// The runtime
// static RT: &'static str = include_str!("rt.rs.t");
// /// Create a tuple of values
// fn tup(x: Vec<String>) -> String{
//     let mut s = "(".to_owned();
//     for y in x{
//         s = format!("{}, {}",s,y);
//     }
//     return s + ")"
// }
// /// Get Rust-compatible variables for some instructions
// fn vars_(x: Vec<crate::ir_untyped::Instruction>) -> Vec<String>{
//     let mut res = vec![];
//     for i in x{
//         for f in i.flow(){
//             for t in f.targets(){
//                 res.push(to_str(t))
//             }
//         }
//     }
//     res
// }
// /// Get a Rust-compatible variable name for a Location
// fn to_str(x: crate::ir_untyped::Location) -> String{
//     return match  x {
//         crate::ir_untyped::Location::Named(x) => format!("{}",x),
//         crate::ir_untyped::Location::Temporary(y) => format!("${}",y),
//         crate::ir_untyped::Location::Literal(l) => format!("{}",l),
//         crate::ir_untyped::Location::Universe(u) => format!("Universe({})",u),
//         crate::ir_untyped::Location::Type(l) => format!("{}$type",to_str(*l)),
//     }
// }
// /// Convert a Vec of Instructions to Rust
// fn insts_(insts: Vec<crate::ir_untyped::Instruction>, vars: Vec<String>) -> String{
//     let mut res = "".to_owned();
//     for i in insts{
//         match i{
//             crate::ir_untyped::Instruction::Apply { target, func, argument } => res = format!("{res}; to<Fun>({}).unwrap()(&{},&mut {})", to_str(func), to_str(argument), to_str(target)),
//             crate::ir_untyped::Instruction::Collect { target, source } => todo!(),
//             crate::ir_untyped::Instruction::CollectTy { target, source } => todo!(),
//             crate::ir_untyped::Instruction::CollectRecord { target, source } => todo!(),
//             crate::ir_untyped::Instruction::DeconstructRecord { target, source, key } => todo!(),
//             crate::ir_untyped::Instruction::Branch { target, conds } => {
//                 res  = format!("{res}; let {} = branch!({}, [", tup(vars.clone()), to_str(target));
//                 for (k,c) in conds{
//                     res = format!("{res} {k}: {{{}; {}}}", insts_(c, vars.clone()), tup(vars.clone()));
//                 }
//                 res += "]);"
//             },
//             crate::ir_untyped::Instruction::Phi { target, sources } => todo!(),
//             crate::ir_untyped::Instruction::DestructureData { source, constructor, targets } => todo!(),
//             crate::ir_untyped::Instruction::DestructureTuple { source, targets } => todo!(),
//             crate::ir_untyped::Instruction::DefLambda { target, param, body } => {
//                 res = format!("{res}; let {} = dynamic!(||({}: &dyn Obj,$ret: &mut dyn Obj){{let {} = {}; {};}});", to_str(target), to_str(param), tup(vars_(body.clone())),tup(vars_(body.clone()).into_iter().map(|x|"*null()".to_owned()).collect()), insts_(body.clone(), vars_(body)));
//             },
//             crate::ir_untyped::Instruction::DefPi { target, param, body } => {
//                 res = format!("{res}; let {} = dynamic!(||({}: &dyn Obj,$ret: &mut dyn Obj){{let {} = {}; {};}});", to_str(target), to_str(param),tup(vars_(body.clone())), tup(vars_(body.clone()).into_iter().map(|x|"*null()".to_owned()).collect()), insts_(body.clone(), vars_(body)));
//             },
//             crate::ir_untyped::Instruction::Return { target } => res = format!("{res}; *$ret = {}; return;", to_str(target)),
//             crate::ir_untyped::Instruction::Unreachable => todo!(),
//             crate::ir_untyped::Instruction::FixType { target, typ } => todo!(),
//         };
//     };
//     return res;
// }