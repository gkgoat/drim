#![allow(missing_docs)]

pub trait RemoveFirst<T> {
    fn remove_first(&mut self) -> Option<T>;
}

impl<T> RemoveFirst<T> for Vec<T> {
    fn remove_first(&mut self) -> Option<T> {
        if self.is_empty() {
            return None;
        }
        Some(self.remove(0))
    }
}
pub fn vals<T,U> (x: Vec<(U,T)>) -> Vec<T>{
    return x.into_iter().map(|(_,b)|b).collect()
}