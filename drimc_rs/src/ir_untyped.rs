//! Untyped intermediate representation.
//!
//! Untyped IR is immediately generated after the AST has been successfully parsed. It dramatically
//! simplifies the complexity of the syntax tree, and is used to perform type inference, at which
//! point it is translated into typed IR.

use std::fmt::Display;

use crate::{
    syntax::{self, Identifier, Literal, Pattern, SyntaxTree, TypeConstructor, TypeName},
    typeck::Type,
};

/// A program represented in untyped IR.
#[derive(Debug)]
pub struct Program {
    /// The list of top-level declarations. Each declaration has a fully-namespaced name, and a list
    /// of instructions that should return the name's value. E.g., for
    /// ```
    /// def x = 5;
    /// ```
    /// the name `x` becomes associated with a list of instructions that return the value 5. When
    /// the program starts up, each one of these top-level functions is immediately called to
    /// initialize global variables, and then `main` is invoked.
    defs: Vec<(Identifier, Vec<Instruction>)>,
}

impl Display for Program {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for def in self.defs.iter() {
            writeln!(f, "PR {}", def.0)?;
            for inst in def.1.iter() {
                writeln!(f, "{}", inst)?;
            }
        }
        Ok(())
    }
}

/// An instruction in untyped IR.
#[derive(Debug, Clone)]
pub(crate) enum Instruction {
    /// Apply a single-argument function to a value, and store the result in another location.
    Apply {
        /// The target into which to store the result.
        target: Location,

        /// The function to call.
        func: Location,

        /// The argument to call the function with.
        argument: Location,

        /// They type of the argument
        ty: Location,
    },

    /// Apply a single argument to a primitive
    ApplyPrim {
        /// The target into which to store the result.
        target: Location,

        /// The function to call.
        func: Identifier,

        /// The argument to call the function with.
        argument: Location,
    },

    /// Collect zero or more values into a storage location as a tuple.
    Collect {
        /// The target into which to store the result.
        target: Location,

        /// The source locations to pull from. If there is exactly one of these, then the variable
        /// is simply moved (since we define that a tuple of one element is the same as just that
        /// element); otherwise, the variables are collected into a tuple and moved into `target`.
        /// Also this means that if `source` is an empty `Vec`, then `target` becomes the unit value
        /// `()`.
        source: Vec<Location>,
    },

    // /// Collect zero or more values into a storage location as a tuple type.
    // CollectTy {
    //     /// The target into which to store the result.
    //     target: Location,

    //     /// The source locations to pull from. If there is exactly one of these, then the variable
    //     /// is simply moved (since we define that a tuple of one element is the same as just that
    //     /// element); otherwise, the variables are collected into a tuple type and moved into `target`.
    //     /// Also this means that if `source` is an empty `Vec`, then `target` becomes the unit value
    //     /// `()`.
    //     source: Vec<Location>,
    // },
    /// Collect zero or more values into a storage location as a record
    CollectRecord {
        /// The target into which to store the result.
        target: Location,

        /// The source locations to pull from. The variables are collected into a record and moved into `target`.
        /// Also this means that if `source` is an empty `Vec`, then `target` becomes the unit value
        /// `{}`.
        source: Vec<(String, Location)>,
    },

    // // Collect zero or more values into a storage location as a record type
    // CollectRecordTy {
    //     /// The target into which to store the result.
    //     target: Location,

    //     /// The source locations to pull from. The variables are collected into a record type and moved into `target`.
    //     /// Also this means that if `source` is an empty `Vec`, then `target` becomes the unit value
    //     /// `{}`.
    //     source: Vec<(String, Location)>,
    // },
    /// Get the value of a key in a record
    DeconstructRecord {
        /// The result
        target: Location,

        /// The record
        source: Location,

        /// The key
        key: String,
    },

    /// Branch depending on which variant of its type a particular storage location is.
    Branch {
        /// The storage location to inspect.
        target: Location,

        /// The conditions to branch on.
        conds: Vec<(Pattern, Vec<Instruction>)>,
    },

    /// Copy a value from one of a number of locations, whichever is initialized.
    Phi {
        /// The location to store the new value in.
        target: Location,

        /// The source locations; exactly one of these must have been initialized.
        sources: Vec<Location>,
    },

    /// Destructure an algebraic object into its component pieces.
    DestructureData {
        /// The object we're destructuring.
        source: Location,

        /// The constructor that the `target` was constructed with. It is *undefined behavior* if
        /// `target` is of a variant type and this is not the constructor `target` was constructed
        /// with; the AST translator must never produce a `DestructureData` node without a guarding
        /// `Branch` node if there exists more than one variant of the underlying type.
        constructor: Identifier,

        /// The list of locations to store the parameters to the constructor. It is a type error
        /// (caught at type checking time) for the number of targets here to be different from the
        /// number of parameters to the constructor.
        targets: Vec<Location>,
    },

    /// Destructure a tuple into its component pieces.
    DestructureTuple {
        /// The tuple we're destructuring.
        source: Location,

        /// The list of locations to store the elements of the tuple. It is a type error (caught at
        /// type checking time) for the number of targets here to be different from the number of
        /// elements in the tuple.
        targets: Vec<Location>,
    },

    /// Create a new lambda with one parameter, and store it in a location.
    DefLambda {
        /// The location to store the lambda in.
        target: Location,

        /// The name of the parameter to the lambda.
        param: Location,

        /// Code to execute when the lambda is called.
        body: Vec<Instruction>,
    },

    // /// Create a new pi type with one parameter, and store it in a location.
    // DefPi {
    //     /// The location to store the lambda in.
    //     target: Location,

    //     /// The name of the parameter to the lambda.
    //     param: Location,

    //     /// Code to execute when the lambda is called.
    //     body: Vec<Instruction>,
    // },
    /// Return a value from a function. It is *undefined behavior* for a function body to not
    /// include a `Return` instruction, and no instructions should ever follow a `Return`
    /// instruction.
    Return {
        /// The location to return.
        target: Location,
    },

    /// Mark this location as unreachable. If this node is reachable at runtime, then the entire
    /// program is undefined behavior.
    Unreachable,

    /// Assert that a storage location is of a particular type. While this node does nothing on its
    /// own, `FixType` is used as the fundamental anchor for type inference: all types in the
    /// program are deduced relative to this node.
    FixType {
        /// The storage location being annotated.
        target: Location,

        /// The type that the storage location should have.
        typ: Location,
    },
}
fn varsP(p: Pattern) -> Vec<Location> {
    return match p {
        Pattern::Capture(x) => vec![Location::Named(Identifier { elems: vec![x] })],
        Pattern::Tuple(xs) => xs.into_iter().flat_map(|y| varsP(y)).collect(),
        Pattern::Record {
            members,
            inexhaustive,
        } => members
            .into_iter()
            .flat_map(|(a, b)| match b {
                None => vec![],
                Some(c) => varsP(c),
            })
            .collect(),
        Pattern::TypeAnnotated { pat, typ } => varsP(*pat),
        Pattern::Destructure(a, b) => varsP(*b),
        Pattern::Ignore => vec![],
        Pattern::Literal(_) => vec![],
    };
}
impl Instruction {
    pub fn targets(self) -> Vec<Location> {
        return match self {
            Instruction::Apply {
                target,
                func,
                argument,
                ty,
            } => vec![target],
            Instruction::ApplyPrim {
                target,
                func,
                argument,
                // ty,
            } => vec![target],
            Instruction::Collect { target, source } => vec![target],
            Instruction::CollectRecord { target, source } => vec![target],
            Instruction::DeconstructRecord {
                target,
                source,
                key,
            } => vec![target],
            Instruction::Branch { target, conds } => vec![]
                .into_iter()
                .chain(conds.into_iter().flat_map(|(p, _)| varsP(p)))
                .collect(),
            Instruction::Phi { target, sources } => vec![target],
            Instruction::DestructureData {
                source,
                constructor,
                targets,
            } => targets,
            Instruction::DestructureTuple { source, targets } => targets,
            Instruction::DefLambda {
                target,
                param,
                body,
            } => vec![target]
                .into_iter()
                .chain(body.into_iter().flat_map(|i| i.targets()))
                .collect(),
            // Instruction::DefPi {
            //     target,
            //     param,
            //     body,
            // } => vec![target]
            //     .into_iter()
            //     .chain(body.into_iter().flat_map(|i| i.targets()))
            //     .collect(),
            Instruction::Return { target } => vec![],
            Instruction::Unreachable => vec![],
            Instruction::FixType { target, typ } => vec![],
        };
    }
    fn sources(self) -> Vec<Location> {
        return match self {
            Instruction::Apply {
                target,
                func,
                argument,
                ty,
            } => vec![func, argument, ty],
            Instruction::ApplyPrim {
                target,
                func,
                argument,
            } => vec![argument, Location::Named(func)],
            Instruction::Collect { target, source } => source,
            Instruction::CollectRecord { target, source } => crate::util::vals(source),
            Instruction::DeconstructRecord {
                target,
                source,
                key,
            } => vec![source],
            Instruction::Branch { target, conds } => vec![target],
            Instruction::Phi { target, sources } => sources,
            Instruction::DestructureData {
                source,
                constructor,
                targets,
            } => vec![source],
            Instruction::DestructureTuple { source, targets } => vec![source],
            Instruction::DefLambda {
                target,
                param,
                body,
            } => body.into_iter().flat_map(|b| b.sources()).collect(),
            Instruction::Return { target } => vec![target],
            Instruction::Unreachable => vec![],
            Instruction::FixType { target, typ } => vec![typ, target],
        };
    }
    pub fn flow(self) -> Vec<Instruction> {
        return vec![self.clone()]
            .into_iter()
            .chain(match self {
                Instruction::Apply {
                    target,
                    func,
                    argument,
                    ty,
                } => vec![],
                Instruction::ApplyPrim {
                    target,
                    func,
                    argument,
                } => vec![],
                Instruction::Collect { target, source } => vec![],
                // Instruction::CollectTy { target, source } => vec![],
                Instruction::CollectRecord { target, source } => vec![],
                Instruction::DeconstructRecord {
                    target,
                    source,
                    key,
                } => vec![],
                Instruction::Branch { target, conds } => conds
                    .into_iter()
                    .flat_map(|(_, i)| i)
                    .flat_map(|x| x.flow())
                    .collect(),
                Instruction::Phi { target, sources } => vec![],
                Instruction::DestructureData {
                    source,
                    constructor,
                    targets,
                } => vec![],
                Instruction::DestructureTuple { source, targets } => vec![],
                Instruction::DefLambda {
                    target,
                    param,
                    body,
                } => body.into_iter().flat_map(|x| x.flow()).collect(),
                // Instruction::DefPi {
                //     target,
                //     param,
                //     body,
                // } => body.into_iter().flat_map(|x| x.flow()).collect(),
                Instruction::Return { target } => vec![],
                Instruction::Unreachable => vec![],
                Instruction::FixType { target, typ } => vec![],
            })
            .collect();
    }
    fn dce_in(
        self,
        i: &[Instruction],
        then: &[Instruction],
        ext: Vec<Location>,
    ) -> Option<Instruction> {
        let srcs = i
            .to_owned()
            .into_iter()
            // .chain(then.iter().map(|y|y.clone()))
            .flat_map(|a| a.flow())
            .flat_map(|f| f.sources())
            .collect::<Vec<_>>();
        for t in self.clone().targets() {
            if srcs.contains(&t) {
                return Some(self);
            }
            if ext.contains(&t) {
                return Some(self);
            }
        }
        if self.clone().targets().len() == 0 {
            return Some(self);
        }
        if let Instruction::Branch { target, conds } = self.clone() {
            return Some(self); //hack;
        };
        // println!("dbg {}",self);
        return None;
    }
    fn dce_start(self, then: &[Instruction], ext: Vec<Location>) -> Instruction {
        return match self.clone() {
            Instruction::Apply {
                target,
                func,
                argument,
                ty,
            } => self,
            Instruction::ApplyPrim {
                target,
                func,
                argument,
                // ty,
            } => self,
            Instruction::Collect { target, source } => self,
            // Instruction::CollectTy { target, source } => self,
            Instruction::CollectRecord { target, source } => self,
            Instruction::DeconstructRecord {
                target,
                source,
                key,
            } => self,
            Instruction::Branch { target, conds } => Instruction::Branch {
                target,
                conds: conds
                    .into_iter()
                    .map(|(a, b)| (a, dce(b, then.to_owned(), ext.clone())))
                    .collect(),
            },
            Instruction::Phi { target, sources } => self,
            Instruction::DestructureData {
                source,
                constructor,
                targets,
            } => self,
            Instruction::DestructureTuple { source, targets } => self,
            Instruction::DefLambda {
                target,
                param,
                body,
            } => Instruction::DefLambda {
                target,
                param,
                body: dce(body, then.to_owned(), ext),
            },
            Instruction::Return { target } => self,
            Instruction::Unreachable => self,
            Instruction::FixType { target, typ } => self,
        };
    }
}
fn dce(
    mut x: Vec<Instruction>,
    mut then: Vec<Instruction>,
    extrn: Vec<Location>,
) -> Vec<Instruction> {
    let y = x.clone();
    let t = then.clone();
    let mut res = vec![];
    x.append(&mut then);
    while x.len() != t.len() {
        if let Some(z) = x.remove(0).dce_in(&x, &x, extrn.clone()) {
            res.push(z.dce_start(&x, extrn.clone()))
        }
    }
    if res.len() != y.len() {
        return dce(res, t, extrn);
    }
    return res;
}

impl Display for Instruction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Instruction::Apply {
                target,
                func,
                argument,
                ty,
            } => write!(f, "AP {target} <- {func} {argument} : {ty}"),
            Instruction::ApplyPrim {
                target,
                func,
                argument,
                // ty,
            } => write!(f, "APP {target} <- {func} {argument}"),
            Instruction::Collect { target, source } => {
                write!(f, "CL {target} <- ")?;
                for s in source {
                    write!(f, "{s},")?;
                }
                Ok(())
            }
            Instruction::CollectRecord { target, source } => {
                write!(f, "CLR {target} <- ")?;
                for (n, s) in source {
                    write!(f, "{n}: {s},")?;
                }
                Ok(())
            }
            Instruction::Branch { target, conds } => {
                writeln!(f, "CASE {target} [");
                for (i, c) in conds {
                    write!(f, "{i}: ");
                    for inst in c {
                        writeln!(f, "{inst}");
                    }
                }
                writeln!(f, "]\n");
                Ok(())
            }
            Instruction::Phi { target, sources } => {
                write!(f, "PH {target} ")?;
                for source in sources {
                    write!(f, "{source},")?;
                }
                Ok(())
            }
            Instruction::DestructureData {
                source,
                constructor,
                targets,
            } => {
                write!(f, "DS {constructor} (")?;
                for target in targets {
                    write!(f, "{target},")?;
                }
                write!(f, ") << {source}")?;
                Ok(())
            }
            Instruction::DestructureTuple { source, targets } => {
                write!(f, "DT (")?;
                for target in targets {
                    write!(f, "{target},")?;
                }
                write!(f, ") <- {source}")?;
                Ok(())
            }
            Instruction::DefLambda {
                target,
                param,
                body,
                // ret
            } => {
                writeln!(f, "DL {target} <- \\{param} [")?;
                for inst in body {
                    writeln!(f, "{}", inst)?;
                }
                write!(f, "]")?;
                Ok(())
            }
            // Instruction::DefPi {
            //     target,
            //     param,
            //     body,
            //     // ret
            // } => {
            //     writeln!(f, "DP {target} <- \\{param} [")?;
            //     for inst in body {
            //         writeln!(f, "{}", inst)?;
            //     }
            //     write!(f, "]")?;
            //     Ok(())
            // }
            Instruction::Return { target } => {
                write!(f, "RT {target}")
            }
            Instruction::Unreachable => {
                write!(f, "UN")
            }
            Instruction::DeconstructRecord {
                target,
                source,
                key,
            } => {
                writeln!(f, "DCR {target} <- {source}.{key}")
            }
            Instruction::FixType { target, typ } => writeln!(f, "FXT {target} : {typ}"),
        }
    }
}

/// A storage location in untyped IR.
#[derive(Debug, Clone, PartialEq)]
pub(crate) enum Location {
    /// A named location. Identifiers can be read from to access globally-defined functions and
    /// constants; but identifiers that are bound to (e.g., the `x` in `let x = 5`) must not be
    /// namespaced.
    Named(Identifier),

    /// A compiler-generated temporary location.
    Temporary(u64),

    /// A constant value.
    Literal(Literal),

    Universe(u64),

    Type(Box<Location>),
}

impl Display for Location {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Location::Named(id) => write!(f, "n`{}`", id),
            Location::Temporary(t) => write!(f, "t`{}`", t),
            Location::Literal(l) => write!(f, "c`{}`", l),
            Location::Universe(u) => write!(f, "u`{}`", u),
            Location::Type(o) => write!(f, "{o}%type"),
        }
    }
}
fn convert_constructor(x: TypeConstructor, counter: &mut u64, l: &Location) -> Vec<Instruction> {
    let mut ts = vec![];
    let mut is = vec![];
    for e in x.args {
        let t = temporary(counter);
        ts.push(t.clone());
        is = is.into_iter().chain(eval_expr(counter, e, &t)).collect();
    }
    is.push(Instruction::Collect {
        target: l.clone(),
        source: ts,
    });
    return is;
}
fn convert_constructors(
    constructors: Vec<TypeConstructor>,
    name: Identifier,
    counter: &mut u64,
) -> Vec<Instruction> {
    let t = temporary(counter);
    let t2 = temporary(counter);
    let mut is = vec![];
    let ts = constructors
        .into_iter()
        .map(|c| {
            let t = temporary(counter);
            is = is
                .clone()
                .into_iter()
                .chain(convert_constructor(c, counter, &t))
                .collect();
            t
        })
        .collect::<Vec<_>>();
    return is
        .into_iter()
        .chain(vec![
            Instruction::Collect {
                target: t2.clone(),
                source: ts,
            },
            Instruction::ApplyPrim {
                target: t.clone(),
                func: Identifier {
                    elems: vec!["core".to_owned(), "Data".to_owned()],
                },
                argument: t2,
            },
            Instruction::Return { target: t },
        ])
        .collect();
}
fn construct(
    ty: TypeName,
    constrs: Vec<TypeConstructor>,
    counter: &mut u64,
) -> (Identifier, Vec<Instruction>) {
    return match ty {
        TypeName::Named(name) => (name.clone(), convert_constructors(constrs, name, counter)),
        TypeName::Application {
            function,
            expression,
        } => match *expression {
            TypeName::Named(tgt) => {
                let p = temporary(counter);
                // let ptb = temporary(counter);
                let mut counter = *counter;
                let (x, insts) = construct(*function, constrs, &mut counter);
                (
                    x.clone(),
                    vec![
                        Instruction::DefLambda {
                            target: p.clone(),
                            param: Location::Named(x.clone()),
                            body: insts,
                        },
                        Instruction::DefLambda {
                            target: ty_of(p.clone()),
                            param: Location::Named(x),
                            body: vec![Instruction::Return {
                                target: Location::Named(Identifier {
                                    elems: vec!["core".to_owned(), "Type".to_owned()],
                                }),
                            }],
                        },
                        Instruction::Return { target: p },
                    ],
                )
            }
            _ => todo!(),
        },
    };
}
/// Converts a program's abstract syntax tree into untyped IR code.
pub fn ast_to_untyped_ir(ast: SyntaxTree) -> Program {
    let mut defs = vec![];
    let mut counter = 0;
    for stmt in ast.0.into_iter() {
        match stmt {
            syntax::Statement::TypeDefinition { typ, constructors } => {
                defs.push(construct(typ, constructors, &mut counter))
            }
            syntax::Statement::InstanceDefinition {
                class_name: _,
                typ: _,
                decls: _,
            } => todo!(),
            syntax::Statement::ClassDefinition {
                name: _,
                var: _,
                decls: _,
            } => todo!(),
            syntax::Statement::ClassMember(syntax::ClassMember::Function {
                name,
                arguments,
                definition,
            }) => {
                defs.push((
                    name,
                    convert_fn(
                        &mut counter,
                        arguments,
                        definition.expect("Empty functions unimplemented"),
                    ),
                ));
            }
            syntax::Statement::ClassMember(syntax::ClassMember::TypeAlias {
                left: _,
                right: _,
            }) => {
                todo!()
            }
        }
    }

    Program { defs }
}

/// Generates a new temporary location name that's guaranteed to be unique.
fn temporary(counter: &mut u64) -> Location {
    let n = *counter;
    *counter += 1;
    Location::Temporary(n)
}

/// Converts a function definition into a list of instructions.
fn convert_fn(
    counter: &mut u64,
    mut arguments: Vec<syntax::Pattern>,
    definition: syntax::Expr,
) -> Vec<Instruction> {
    dce(
        if arguments.is_empty() {
            let ret_loc = temporary(counter);
            eval_expr(counter, definition, &ret_loc)
                .into_iter()
                .chain(std::iter::once(Instruction::Return { target: ret_loc }))
                .collect()
        } else {
            let first = arguments.remove(0);

            let lambda_loc = temporary(counter);
            let arg_loc = temporary(counter);
            vec![
                Instruction::DefLambda {
                    target: lambda_loc.clone(),
                    param: arg_loc.clone(),
                    body: bind_pattern(counter, first, &arg_loc)
                        .into_iter()
                        .chain(convert_fn(counter, arguments, definition))
                        .collect(),
                },
                Instruction::Return { target: lambda_loc },
            ]
        },
        vec![],
        vec![],
    )
}

/// Emits instructions that bind the given pattern to the variable stored in location `l`.
fn bind_pattern(counter: &mut u64, p: syntax::Pattern, l: &Location) -> Vec<Instruction> {
    let q = p.clone();
    match p {
        syntax::Pattern::Capture(name) => {
            vec![
                Instruction::Collect {
                    target: Location::Named(syntax::Identifier {
                        elems: vec![name.clone()],
                    }),
                    source: vec![l.to_owned()],
                },
                Instruction::Collect {
                    target: ty_of(Location::Named(syntax::Identifier { elems: vec![name] })),
                    source: vec![ty_of(l.to_owned())],
                },
            ]
        }
        syntax::Pattern::Tuple(pats) => {
            let pat_locs: Vec<_> = pats.iter().map(|_| temporary(counter)).collect();
            vec![
                Instruction::DestructureTuple {
                    source: l.to_owned(),
                    targets: pat_locs.clone(),
                },
                Instruction::DestructureTuple {
                    source: ty_of(l.to_owned()),
                    targets: pat_locs.clone().into_iter().map(ty_of).collect(),
                },
            ]
            .into_iter()
            .chain(
                Iterator::zip(pats.into_iter(), pat_locs)
                    .map(|(pat, pat_loc)| bind_pattern(counter, pat, &pat_loc))
                    .flatten(),
            )
            .collect()
        }
        syntax::Pattern::Record {
            members,
            inexhaustive,
        } => members
            .into_iter()
            .flat_map(|(f, p)| match p {
                None => vec![],
                Some(p) => {
                    let r = temporary(counter);
                    let pre = vec![
                        Instruction::DeconstructRecord {
                            target: r.clone(),
                            source: l.clone(),
                            key: f.clone(),
                        },
                        Instruction::DeconstructRecord {
                            target: ty_of(r.clone()),
                            source: ty_of(l.clone()),
                            key: f,
                        },
                    ];
                    let post = bind_pattern(counter, p, &r);
                    return pre.into_iter().chain(post).collect();
                }
            })
            .collect(),
        syntax::Pattern::TypeAnnotated { pat, typ } => {
            let tr = temporary(counter);
            let ta = eval_expr(counter, *typ, &tr);
            ta.into_iter().chain(vec![
                Instruction::FixType {
                    target: l.to_owned(),
                    // typ: *typ,
                    typ: tr.clone(),
                },
                Instruction::Collect {
                    target: ty_of(l.to_owned()),
                    source: vec![tr],
                },
            ])
        }
        .chain(bind_pattern(counter, *pat, l))
        .collect(),
        syntax::Pattern::Destructure(i, p) => {
            let u = temporary(counter);
            let up = bind_pattern(counter, *p, &u);
            return up
                .into_iter()
                .chain(vec![
                    Instruction::DestructureData {
                        source: u.clone(),
                        constructor: q.clone().constructor(),
                        targets: vec![l.clone()],
                    },
                    Instruction::DestructureData {
                        source: ty_of(u),
                        constructor: q.constructor(),
                        targets: vec![ty_of(l.clone())],
                    },
                ])
                .collect();
        }
        syntax::Pattern::Ignore => Vec::new(),
        syntax::Pattern::Literal(_) => Vec::new(),
    }
}

/// Emits instructions that check whether the expression `e` matches the pattern, and if so,
/// evaluates `e1` and places the result in `l`; otherwise, evaluates `e2` and places the result in
/// `l`.
fn conditional(
    counter: &mut u64,
    e: syntax::Expr,
    p: syntax::Pattern,
    e1: syntax::Expr,
    e2: syntax::Expr,
    l: &Location,
) -> Vec<Instruction> {
    todo!()
}

/// Emits instructions that evaluate the expression `e`, then place the result in location `l`.
fn eval_expr(counter: &mut u64, e: syntax::Expr, l: &Location) -> Vec<Instruction> {
    match e {
        syntax::Expr::BinaryOp {
            kind: _,
            left,
            right,
            translation,
        } => eval_expr(
            counter,
            syntax::Expr::Application {
                func: Box::new(syntax::Expr::Application {
                    func: Box::new(syntax::Expr::VariableReference(Identifier {
                        elems: vec![translation],
                    })),
                    argument: left,
                }),
                argument: right,
            },
            l,
        ),
        syntax::Expr::Application { func, argument } => {
            let func_e = temporary(counter);
            let arg_e = temporary(counter);
            Iterator::chain(
                eval_expr(counter, *func, &func_e).into_iter(),
                eval_expr(counter, *argument, &arg_e).into_iter(),
            )
            .chain(vec![
                Instruction::Apply {
                    target: l.to_owned(),
                    func: func_e.clone(),
                    argument: arg_e.clone(),
                    ty: ty_of(arg_e.clone()),
                },
                Instruction::Apply {
                    target: ty_of(l.to_owned()),
                    func: ty_of(func_e),
                    argument: arg_e.clone(),
                    ty: ty_of(arg_e.clone()),
                },
            ])
            .collect()
        }
        syntax::Expr::Let { left, right, into } => {
            let right_e = temporary(counter);
            let eval_right = eval_expr(counter, *right, &right_e);
            let bind_left = bind_pattern(counter, left, &right_e);
            let eval_into = eval_expr(counter, *into, l);
            eval_right
                .into_iter()
                .chain(bind_left)
                .chain(eval_into)
                .collect()
        }
        syntax::Expr::Match { matcher, cases } => {
            let res = temporary(counter);
            let eval_target = eval_expr(counter, *matcher, &res);
            let c = cases
                .clone()
                .into_iter()
                .map(|(a, x)| {
                    let bind = bind_pattern(counter, a.clone(), &res);
                    (
                        a,
                        bind.into_iter()
                            .chain(eval_expr(counter, x, l))
                            .collect::<Vec<_>>(),
                    )
                })
                .collect::<Vec<_>>();
            eval_target
                .into_iter()
                .chain(vec![Instruction::Branch {
                    target: res.clone(),
                    conds: c.clone(),
                }])
                .collect()
        }
        syntax::Expr::Record(r) => {
            let mut res = vec![];
            let mut locs = vec![];
            for (k, e) in r {
                let r = temporary(counter);
                locs.push((k, r.clone()));
                res = res.into_iter().chain(eval_expr(counter, e, &r)).collect();
            }
            res.push(Instruction::CollectRecord {
                target: l.clone(),
                source: locs.clone(),
            });
            res.push(Instruction::CollectRecord {
                target: Location::Type(Box::new(l.clone())),
                source: locs.into_iter().map(|(y, x)| (y, ty_of(x))).collect(),
            });
            res
        }
        syntax::Expr::Lambda { arguments, result } => {
            if arguments.len() == 1 {
                // let fn_ = temporary(counter)
                let p = temporary(counter);
                let mut counter = *counter;
                let arg = arguments[0].to_owned();
                let arg_e = temporary(&mut counter);
                let r = temporary(&mut counter);
                let bind_arg = bind_pattern(&mut counter, arg, &arg_e);
                let res = eval_expr(&mut counter, *result, &r);
                let r2 = bind_arg
                    .into_iter()
                    .chain(res)
                    .collect::<Vec<Instruction>>();
                // let p = temporary(counter);
                vec![
                    Instruction::DefLambda {
                        target: l.to_owned(),
                        param: arg_e.clone(),
                        body: r2
                            .clone()
                            .into_iter()
                            .chain(vec![Instruction::Return { target: r.clone() }])
                            .collect(),
                    },
                    Instruction::DefLambda {
                        target: p.clone(),
                        param: arg_e,
                        body: r2
                            .into_iter()
                            .chain(vec![Instruction::Return { target: ty_of(r) }])
                            .collect(),
                    },
                    Instruction::ApplyPrim {
                        target: Location::Type(Box::new(l.clone())),
                        func: Identifier {
                            elems: vec!["core".to_owned(), "Pi".to_owned()],
                        },
                        argument: p,
                    },
                ]
            } else {
                todo!()
            }
        }
        syntax::Expr::Pi { arguments, result } => {
            if arguments.len() == 1 {
                // let fn_ = temporary(counter)
                let p = temporary(counter);
                let mut counter = *counter;
                let arg = arguments[0].to_owned();
                let arg_e = temporary(&mut counter);
                let r = temporary(&mut counter);
                let bind_arg = bind_pattern(&mut counter, arg, &arg_e);
                let res = eval_expr(&mut counter, *result, &r);
                let r2 = bind_arg.into_iter().chain(res).collect::<Vec<_>>();
                vec![
                    Instruction::DefLambda {
                        target: p.clone(),
                        param: arg_e,
                        body: r2
                            .clone()
                            .into_iter()
                            .chain(vec![Instruction::Return { target: r.clone() }])
                            .collect(),
                    },
                    Instruction::Collect {
                        target: ty_of(l.clone()),
                        source: vec![Location::Universe(0)],
                    },
                    Instruction::ApplyPrim {
                        target: l.clone(),
                        func: Identifier {
                            elems: vec!["core".to_owned(), "Pi".to_owned()],
                        },
                        argument: p,
                    },
                ]
            } else {
                todo!()
            }
        }
        syntax::Expr::DotSubscript { value, subscript } => match *subscript {
            syntax::Expr::VariableReference(name) => {
                let c = temporary(counter);
                let ia = eval_expr(counter, *value, &c);
                let ib = vec![
                    Instruction::DeconstructRecord {
                        target: l.clone(),
                        source: c.clone(),
                        key: name.elems[0].clone(),
                    },
                    Instruction::DeconstructRecord {
                        target: ty_of(l.clone()),
                        source: ty_of(c),
                        key: name.elems[0].clone(),
                    },
                ];
                ia.into_iter().chain(ib).collect()
            }
            _ => todo!(),
        },
        syntax::Expr::BracketSubscript { value, subscript } => todo!(),
        syntax::Expr::Tuple(elems) => {
            let elem_locs: Vec<_> = elems.iter().map(|_| temporary(counter)).collect();
            let p = temporary(counter);
            elems
                .into_iter()
                .zip(elem_locs.iter())
                .map(|(elem, elem_loc)| eval_expr(counter, elem, elem_loc))
                .flatten()
                .chain(vec![
                    Instruction::Collect {
                        target: l.to_owned(),
                        source: elem_locs.clone(),
                    },
                    Instruction::Collect {
                        target: p.clone(),
                        source: elem_locs.clone().into_iter().map(|l| ty_of(l)).collect(),
                    },
                    Instruction::ApplyPrim {
                        target: ty_of(l.clone()),
                        func: Identifier {
                            elems: vec!["core".to_owned(), "Tup".to_owned()],
                        },
                        argument: p,
                    },
                ])
                .collect()
        }
        syntax::Expr::Ty(t) => match t {
            syntax::Type::Tuple(elems) => {
                let elem_locs: Vec<_> = elems.iter().map(|_| temporary(counter)).collect();
                let p = temporary(counter);
                elems
                    .into_iter()
                    .zip(elem_locs.iter())
                    .map(|(elem, elem_loc)| eval_expr(counter, elem, elem_loc))
                    .flatten()
                    .chain(vec![
                        Instruction::Collect {
                            target: p.clone(),
                            source: elem_locs.clone(),
                        },
                        Instruction::ApplyPrim {
                            target: l.clone(),
                            func: Identifier {
                                elems: vec!["core".to_owned(), "Tup".to_owned()],
                            },
                            argument: p,
                        },
                    ])
                    .collect()
            }
            syntax::Type::Record(elems) => {
                let elem_locs: Vec<_> = elems
                    .iter()
                    .map(|(key, _)| (key.to_owned(), temporary(counter)))
                    .collect();
                let p = temporary(counter);
                elems
                    .into_iter()
                    .zip(elem_locs.iter())
                    .map(|((k_, elem), (_, elem_loc))| eval_expr(counter, elem, elem_loc))
                    .flatten()
                    .chain(vec![
                        Instruction::CollectRecord {
                            target: p.clone(),
                            source: elem_locs.clone(),
                        },
                        Instruction::ApplyPrim {
                            target: l.clone(),
                            func: Identifier {
                                elems: vec!["core".to_owned(), "Rec".to_owned()],
                            },
                            argument: p,
                        },
                    ])
                    .collect()
            }
        },
        syntax::Expr::VariableReference(name) => vec![
            Instruction::Collect {
                target: l.clone(),
                source: vec![Location::Named(name.clone())],
            },
            Instruction::Collect {
                target: ty_of(l.clone()),
                source: vec![ty_of(Location::Named(name))],
            },
        ],
        syntax::Expr::Literal(lit) => vec![Instruction::Collect {
            target: l.clone(),
            source: vec![Location::Literal(lit)],
        }],
        syntax::Expr::TyOf { val } => {
            let r = temporary(counter);
            let s = eval_expr(counter, *val, &r);
            return s
                .into_iter()
                .chain(vec![
                    Instruction::Collect {
                        target: l.clone(),
                        source: vec![ty_of(r.clone())],
                    },
                    Instruction::Collect {
                        target: ty_of(l.clone()),
                        source: vec![ty_of(ty_of(r.clone()))],
                    },
                ])
                .collect();
        }
    }
}
fn ret_tys(x: Vec<Instruction>) -> Vec<Instruction> {
    return x
        .into_iter()
        .map(|y| match y {
            Instruction::Return { target } => Instruction::Return {
                target: ty_of(target),
            },
            _ => y,
        })
        .collect();
}
fn ty_of(target: Location) -> Location {
    match target {
        Location::Type(_) => Location::Universe(0),
        Location::Universe(n) => Location::Universe(n + 1),
        _ => Location::Type(Box::new(target)),
    }
}
